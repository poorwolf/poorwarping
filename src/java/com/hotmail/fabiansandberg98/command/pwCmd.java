package com.hotmail.fabiansandberg98.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.Main;

public class pwCmd implements CommandExecutor {
	
	Main plugin;
	public pwCmd(Main plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label,
			String[] args) {
		
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command can only be used in-game!");
			return true;
		}
		
		Player player = (Player) sender;
		
		if (args.length == 0) {
			player.sendMessage(ChatColor.GREEN + "== (PoorWarps) ==\n"
					+ "/pw add <warp>\n"
					+ "/pw warp <warp>\n"
					+ "/pw remove <warp>\n"
					+ "/pw list");
			return true;
		}
		
		switch (args[0]) {
		case "add":
			if (args.length == 2) {
				if (!player.hasPermission("pw.add")) {
					player.sendMessage(ChatColor.RED + "You don not have permission to do that!");
					return true;
				}
			
				if (plugin.getConfig().contains("warps." + args[1])) {
					player.sendMessage(ChatColor.RED + "This warp already exists!");
					return true;
				}
				
				if (args[1].matches("^.*[^0-9a-zA-Z ].*$")) {
					player.sendMessage(ChatColor.RED + "You can not use special characters!");
					return true;
				}
				
				if ((args[1].length() >= 16)) {
					player.sendMessage(ChatColor.RED + "You can not have more than 15 characters in the warp name!");
					return true;
				}
			
				plugin.getConfig().set("warps." + args[1].toLowerCase() + ".world", player.getLocation().getWorld().getName());
				plugin.getConfig().set("warps." + args[1].toLowerCase() + ".x", player.getLocation().getBlockX());
				plugin.getConfig().set("warps." + args[1].toLowerCase() + ".y", player.getLocation().getBlockY());
				plugin.getConfig().set("warps." + args[1].toLowerCase() + ".z", player.getLocation().getBlockZ());
				plugin.getConfig().set("warps." + args[1].toLowerCase() + ".ya", player.getLocation().getYaw());
				plugin.getConfig().set("warps." + args[1].toLowerCase() + ".pi", player.getLocation().getPitch());
				plugin.saveConfig();
				player.sendMessage(ChatColor.GREEN + "You created a new warp");
				return true;
			} else {
				player.sendMessage(ChatColor.RED + "Wrong usage! Type /pw add <warp>");
				return true;
			}
			
		case "warp":
			if (args.length == 2) {
				if (!player.hasPermission("pw.warp")) {
					player.sendMessage(ChatColor.RED + "You do not have permission to do that!");
					return true;
				}
				
				String warp = args[1].toLowerCase();
				if (!plugin.getConfig().contains("warps." + warp)) {
					player.sendMessage(ChatColor.RED + "The warp does not exist!");
					return true;
				}
				
				String cworld = plugin.getConfig().getString("warps." + warp + ".world");
				World world = Bukkit.getWorld(cworld);
				double x = plugin.getConfig().getInt("warps." + warp + ".x") + 0.5,
						y = plugin.getConfig().getInt("warps." + warp + ".y") + 0.5,
						z = plugin.getConfig().getInt("warps." + warp + ".z") + 0.5;
				
				float ya = plugin.getConfig().getInt("warps." + warp + ".ya"),
						pi = plugin.getConfig().getInt("warps." + warp + ".pi");
				
				player.teleport(new Location(world, x, y, z, ya, pi));
				player.sendMessage(ChatColor.GREEN + "You got teleported to warp " + warp);
				return true;
				
			} else {
				player.sendMessage(ChatColor.RED + "Wrong usage! Type /pw warp <warp>");
				return true;
			}
			
		case "remove":
			if (args.length == 2) {
				if (!player.hasPermission("pw.remove")) {
					player.sendMessage(ChatColor.RED + "You do not have permission to do that!");
					return true;
				}
				
				String warp = args[1].toLowerCase();
				if (!plugin.getConfig().contains("warps." + warp)) {
					player.sendMessage(ChatColor.RED + "The warp does not exist!");
					return true;
				}
				plugin.getConfig().set("warps." + warp, null);
				plugin.saveConfig();
				
				player.sendMessage(ChatColor.GREEN + "You removed warp " + warp);
				return true;
				
			} else {
				player.sendMessage(ChatColor.RED + "Wrong usage! Type /pw remove <warp>");
				return true;
			}
			
		case "list":
			if (!player.hasPermission("pw.list")) {
				player.sendMessage(ChatColor.RED + "You do not have permission to do that!");
				return true;
			}
			
			if (args.length > 1) {
				player.sendMessage(ChatColor.RED + "Wrong usage! Type /pw list");
				return true;
			}
			
			if (plugin.getConfig().getString("warps") == null || plugin.getConfig().getConfigurationSection("warps").getKeys(false).size() == 0) {
				player.sendMessage(ChatColor.RED + "There is no warps!");
				return true;
			}
			
			player.sendMessage(ChatColor.GREEN + "All available warps(" + plugin.getConfig().getConfigurationSection("warps").getKeys(false).size() + "): " + plugin.getConfig().getConfigurationSection("warps").getKeys(false).toString().replace("[", "").replace("]", ""));
			return true;
			
		}
		
		return false;
	}

}
