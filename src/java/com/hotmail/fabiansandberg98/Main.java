package com.hotmail.fabiansandberg98;

import org.bukkit.plugin.java.JavaPlugin;

import com.hotmail.fabiansandberg98.command.pwCmd;

public class Main extends JavaPlugin {
	
	@Override
	public void onDisable() {
		getLogger().info("PoorWarps has been disabled!");
	}
	
	@Override
	public void onEnable() {
		
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		getLogger().info("PoorWarps has been enabled!");
		
		getCommand("pw").setExecutor(new pwCmd(this));
		
	}

}
